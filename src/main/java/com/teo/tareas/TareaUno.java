package com.teo.tareas;

import javax.swing.*;

public class TareaUno {

    public static void main(String[] args) {
        String nombre = JOptionPane.showInputDialog("Nombre de usuario");
        int celular = Integer.parseInt(JOptionPane.showInputDialog("celular"));
        int edad = Integer.parseInt(JOptionPane.showInputDialog("Edad"));

        System.out.println("Bienvenido señor "+nombre+", es un placer para nosotros contar con una persona de "+edad+" edad\n");
        System.out.println("Proximamente nos comunicaremos con usted al número "+celular+"\n");
        System.out.println("Feliz tarde ...=) ");

    }

}
